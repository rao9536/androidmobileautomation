
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.URL;

public class TestCases {

    public static AndroidDriver<WebElement> driver;
    public static URL url;

    public static String playStoreAppPackageName="com.android.vending";
    public static String playStoreAppActivityName="com.android.vending.AssetBrowserActivity";

    public static String sauceLabAppPackageName="com.swaglabsmobileapp";
    public static String sauceLabAppActivityName="com.swaglabsmobileapp.MainActivity";

    public static Activity activity;


    @BeforeTest
    public void setup(){

        try {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability(MobileCapabilityType.AUTOMATION_NAME,"Appium");
            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "ANDROID");
            caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "8.0");
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Pixel");
            caps.setCapability(MobileCapabilityType.UDID, "emulator-5554");
            caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
            caps.setCapability("appPackage",sauceLabAppPackageName);
            caps.setCapability("appActivity",sauceLabAppActivityName);

            url = new URL("http://127.0.0.1:4723/wd/hub");
            driver = new AndroidDriver<>(url, caps);


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @AfterTest
    public void tearDown(){
        driver.quit();
    }

    @Test(priority = 10)
    public void ALoginApplication() throws InterruptedException {
        driver.findElement(By.xpath("//android.widget.EditText[@content-desc=\"test-Username\"]")).sendKeys("standard_user");
        driver.findElement(By.xpath("//android.widget.EditText[@content-desc=\"test-Password\"]")).sendKeys("secret_sauce");
        driver.findElement(By.xpath("//android.view.ViewGroup[@content-desc=\"test-LOGIN\"]/android.widget.TextView")).click();
        Thread.sleep(2000);
    }

    @Test(priority = 9)
    public void BchangeView() throws InterruptedException {

        driver.findElement(By.xpath("//android.view.ViewGroup[@content-desc=\"test-Toggle\"]/android.widget.ImageView")).click();
        Thread.sleep(2000);
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"© 2019 Sauce Labs. All Rights Reserved.\"))");
        Thread.sleep(2000);
    }


    @Test(priority = 8)
    public void CclickLastElement() throws InterruptedException {
        driver.findElement (By.xpath ("(//android.view.ViewGroup[@content-desc=\"test-Item\"])[last()]")).click();// need to update this step
        Thread.sleep(2000);
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"ADD TO CART\"))");
        Thread.sleep(2000);
    }


    @Test(priority = 7)
    public void DaddItemToCart() throws InterruptedException {
        driver.findElement(By.xpath("//android.view.ViewGroup[@content-desc=\"test-ADD TO CART\"]/android.widget.TextView")).click();
        Thread.sleep(2000);
    }

   /* @Test(priority = 6)
    public void ElaunchPlayStore() throws InterruptedException {
        activity = new Activity(playStoreAppPackageName,playStoreAppActivityName);
        activity.setStopApp(false);
        ((AndroidDriver<WebElement>) driver).startActivity(activity);
        Thread.sleep(2000);
    }

    @Test(priority = 5)
    public void FrelaunchSauceLab() throws InterruptedException {
        activity = new Activity(sauceLabAppPackageName,sauceLabAppActivityName);
        activity.setStopApp(false);
        ((AndroidDriver<WebElement>) driver).startActivity(activity);
        Thread.sleep(2000);
    }*/

    @Test(priority = 4)
    public void GverifyCartQuantity() throws InterruptedException {
        driver.findElement(By.xpath("//android.view.ViewGroup[@content-desc=\"test-Cart\"]/android.view.ViewGroup/android.widget.ImageView")).click();
        Thread.sleep(2000);
        String size = driver.findElement(By.xpath("//android.view.ViewGroup[@content-desc=\"test-Amount\"]/android.widget.TextView")).getText();
        Assert.assertEquals(size,"1");
        Thread.sleep(2000);
    }
}

